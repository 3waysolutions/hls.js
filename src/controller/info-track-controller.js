import Event from '../events';
import { logger } from '../utils/logger';
import EventHandler from '../event-handler';
import { computeReloadInterval } from './level-helper';

class InfoTrackController extends EventHandler {
  constructor (hls) {
    super(hls,
      Event.MEDIA_ATTACHED,
      Event.MEDIA_DETACHING,
      Event.MANIFEST_LOADED,
      Event.INFO_TRACK_LOADED);
    this.tracks = [];
    this.trackId = -1;
    this.media = null;
    this.stopped = true;

    /**
     * @member {boolean} infoDisplay Enable/disable info display rendering
     */
    this.infoDisplay = true;

    /**
     * Keeps reference to a default track id when media has not been attached yet
     * @member {number}
     */
    this.queuedDefaultTrack = null;
  }

  destroy () {
    EventHandler.prototype.destroy.call(this);
  }

  onMediaAttached (data) {
    this.media = data.media;
  }

  onMediaDetaching () {
    if (!this.media) {
      return;
    }

    if (Number.isFinite(this.infoTrack)) {
      this.queuedDefaultTrack = this.infoTrack;
    }


    // Disable all info tracks before detachment so when reattached only tracks in that content are enabled.
    this.infoTrack = -1;
    this.media = null;
  }

  // Fired whenever a new manifest is loaded.
  onManifestLoaded (data) {
    let tracks = data.infos || [];
    this.tracks = tracks;
    this.hls.trigger(Event.INFO_TRACKS_UPDATED, { infoTracks: tracks });

    // loop through available info tracks and autoselect default if needed
    // TODO: improve selection logic to handle forced, etc
    tracks.forEach(track => {
      if (track.default) {
        // setting this.infoTrack will trigger internal logic
        // if media has not been attached yet, it will fail
        // we keep a reference to the default track id
        // and we'll set infoTrack when onMediaAttached is triggered
        if (this.media) {
          this.infoTrack = track.id;
        } else {
          this.queuedDefaultTrack = track.id;
        }
      }
    });
  }

  onInfoTrackLoaded (data) {
    const { id, details } = data;
    const { trackId, tracks } = this;
    const currentTrack = tracks[trackId];
    if (id >= tracks.length || id !== trackId || !currentTrack || this.stopped) {
      this._clearReloadTimer();
      return;
    }

    logger.log(`info track ${id} loaded`);
    if (details.live) {
      const reloadInterval = computeReloadInterval(currentTrack.details, details, data.stats.trequest);
      logger.log(`Reloading live subtitle playlist in ${reloadInterval}ms`);
      this.timer = setTimeout(() => {
        this._loadCurrentTrack();
      }, reloadInterval);
    } else {
      this._clearReloadTimer();
    }
  }

  startLoad () {
    this.stopped = false;
    this._loadCurrentTrack();
  }

  stopLoad () {
    this.stopped = true;
    this._clearReloadTimer();
  }

  /** get alternate info tracks list from playlist **/
  get infoTracks () {
    return this.tracks;
  }

  /** get index of the selected info track (index in info track lists) **/
  get infoTrack () {
    return this.trackId;
  }

  /** select a info track, based on its index in info track lists**/
  set infoTrack (infoTrackId) {
    if (this.trackId !== infoTrackId) {
      // this._toggleTrackModes(infoTrackId);
      this._setInfoTrackInternal(infoTrackId);
    }
  }

  _clearReloadTimer () {
    if (this.timer) {
      clearTimeout(this.timer);
      this.timer = null;
    }
  }

  _loadCurrentTrack () {
    const { trackId, tracks, hls } = this;
    const currentTrack = tracks[trackId];
    if (trackId < 0 || !currentTrack || (currentTrack.details && !currentTrack.details.live)) {
      return;
    }
    logger.log(`Loading info track ${trackId}`);
    hls.trigger(Event.INFO_TRACK_LOADING, { url: currentTrack.url, id: trackId });
  }

  /**
     * This method is responsible for validating the info index and periodically reloading if live.
     * Dispatches the INFO_TRACK_SWITCH event, which instructs the info-stream-controller to load the selected track.
     * @param newId - The id of the info track to activate.
     */
  _setInfoTrackInternal (newId) {
    const { hls, tracks } = this;
    if (!Number.isFinite(newId) || newId < -1 || newId >= tracks.length) {
      return;
    }

    this.trackId = newId;
    logger.log(`Switching to info track ${newId}`);
    hls.trigger(Event.INFO_TRACK_SWITCH, { id: newId });
    this._loadCurrentTrack();
  }
}

export default InfoTrackController;
