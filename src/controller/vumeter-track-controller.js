import Event from '../events';
import { logger } from '../utils/logger';
import EventHandler from '../event-handler';
import { computeReloadInterval } from './level-helper';

class VumeterTrackController extends EventHandler {
  constructor (hls) {
    super(hls,
      Event.MEDIA_ATTACHED,
      Event.MEDIA_DETACHING,
      Event.MANIFEST_LOADED,
      Event.VUMETER_TRACK_LOADED);
    this.tracks = [];
    this.trackId = -1;
    this.media = null;
    this.stopped = true;

    /**
     * @member {boolean} vumeterDisplay Enable/disable vumeter display rendering
     */
    this.vumeterDisplay = true;

    /**
     * Keeps reference to a default track id when media has not been attached yet
     * @member {number}
     */
    this.queuedDefaultTrack = null;
  }

  destroy () {
    EventHandler.prototype.destroy.call(this);
  }

  onMediaAttached (data) {
    this.media = data.media;
  }

  onMediaDetaching () {
    if (!this.media) {
      return;
    }

    if (Number.isFinite(this.vumeterTrack)) {
      this.queuedDefaultTrack = this.vumeterTrack;
    }


    // Disable all vumeter tracks before detachment so when reattached only tracks in that content are enabled.
    this.vumeterTrack = -1;
    this.media = null;
  }

  // Fired whenever a new manifest is loaded.
  onManifestLoaded (data) {
    let tracks = data.vumeters || [];
    this.tracks = tracks;
    this.hls.trigger(Event.VUMETER_TRACKS_UPDATED, { vumeterTracks: tracks });

    // loop through available vumeter tracks and autoselect default if needed
    // TODO: improve selection logic to handle forced, etc
    tracks.forEach(track => {
      if (track.default) {
        // setting this.vumeterTrack will trigger internal logic
        // if media has not been attached yet, it will fail
        // we keep a reference to the default track id
        // and we'll set vumeterTrack when onMediaAttached is triggered
        if (this.media) {
          this.vumeterTrack = track.id;
        } else {
          this.queuedDefaultTrack = track.id;
        }
      }
    });
  }

  onVumeterTrackLoaded (data) {
    const { id, details } = data;
    const { trackId, tracks } = this;
    const currentTrack = tracks[trackId];
    if (id >= tracks.length || id !== trackId || !currentTrack || this.stopped) {
      this._clearReloadTimer();
      return;
    }

    logger.log(`vumeter track ${id} loaded`);
    if (details.live) {
      const reloadInterval = computeReloadInterval(currentTrack.details, details, data.stats.trequest);
      logger.log(`Reloading live vumeter playlist in ${reloadInterval}ms`);
      this.timer = setTimeout(() => {
        this._loadCurrentTrack();
      }, reloadInterval);
    } else {
      this._clearReloadTimer();
    }
  }

  startLoad () {
    this.stopped = false;
    this._loadCurrentTrack();
  }

  stopLoad () {
    this.stopped = true;
    this._clearReloadTimer();
  }

  /** get alternate vumeter tracks list from playlist **/
  get vumeterTracks () {
    return this.tracks;
  }

  /** get index of the selected vumeter track (index in vumeter track lists) **/
  get vumeterTrack () {
    return this.trackId;
  }

  /** select a vumeter track, based on its index in vumeter track lists**/
  set vumeterTrack (vumeterTrackId) {
    if (this.trackId !== vumeterTrackId) {
      // this._toggleTrackModes(vumeterTrackId);
      this._setVumeterTrackInternal(vumeterTrackId);
    }
  }

  _clearReloadTimer () {
    if (this.timer) {
      clearTimeout(this.timer);
      this.timer = null;
    }
  }

  _loadCurrentTrack () {
    const { trackId, tracks, hls } = this;
    const currentTrack = tracks[trackId];
    if (trackId < 0 || !currentTrack || (currentTrack.details && !currentTrack.details.live)) {
      return;
    }
    logger.log(`Loading vumeter track ${trackId}`);
    hls.trigger(Event.VUMETER_TRACK_LOADING, { url: currentTrack.url, id: trackId });
  }

  /**
     * This method is responsible for validating the vumeter index and periodically reloading if live.
     * Dispatches the VUMETER_TRACK_SWITCH event, which instructs the vumeter-stream-controller to load the selected track.
     * @param newId - The id of the vumeter track to activate.
     */
  _setVumeterTrackInternal (newId) {
    const { hls, tracks } = this;
    if (!Number.isFinite(newId) || newId < -1 || newId >= tracks.length) {
      return;
    }

    this.trackId = newId;
    logger.log(`Switching to vumeter track ${newId}`);
    hls.trigger(Event.VUMETER_TRACK_SWITCH, { id: newId });
    this._loadCurrentTrack();
  }
}

export default VumeterTrackController;
